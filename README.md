# test_tdd_jest_intro

![Gif](https://media.giphy.com/media/GtVfmgTE2Ynf1V299u/giphy.gif)

- Las pruebas TDD son pruebas unitarias.
- Se trata de pruebas de software en el que se prueban unidades/componentes individuales de un software
- El propósito es validar que cada unidad del software funciona como se ha diseñado.
- Primero se crearán las pruebas
- Estas pruebas nos dirán lo que el código debe hacer:

  - Añade una prueba rápida - Básicamente el código suficiente para que falle.
  - Ejecuta la prueba - Debería fallar.
  - Escribe el código funcional, mínimo, para pasar la prueba.
  - Ejecuta la prueba para comprobar si ha pasado.
  - Refactorizar hasta que las pruebas PASEN para continuar con el desarrollo.

  ***

  ![Image](src/assets/flow1.png)

---

Tu turno:

- Prueba a  hacer lo mismo con los diferentes métodos para la [calculadora](https://gitlab.com/jupiyo/test_tdd_jest_intro/-/blob/feature/intro_tdd/src/cases/Calculator.js) (resta, división, multiplicación...)
- Implementa primero la prueba, la cual debe fallar,  y después la funcionalidad.
- Haz lo mínimo para que el test pase. Recuerda: no hay que ser demasiado inteligente ni tampoco demasiado simple.
- Una vez pase el test, habiendo hecho lo mínimo, intenta hacer un refactor
